﻿using System;
using System.IO.Ports;
using System.Threading;

public class PortChat
{
    private static bool _continue;
    private static SerialPort _serialPort;

    public static void Main()
    {
        StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
        Thread readThread = new Thread(Read);

        // Create a new SerialPort object
        _serialPort = new SerialPort("COM2") {ReadTimeout = 500, WriteTimeout = 500};
        _serialPort.Open();
        _continue = true;
        readThread.Start();

        Console.Write("Name: ");
        string name = Console.ReadLine();

        Console.WriteLine("Type QUIT to exit");

        while (_continue)
        {
            string message = Console.ReadLine();

            if (stringComparer.Equals("quit", message))
            {
                _continue = false;
            }
            else
            {
                _serialPort.WriteLine(string.Format($"<{name}>: {message}"));
            }
        }

        readThread.Join();
        _serialPort.Close();
    }

    private static void Read()
    {
        while (_continue)
        {
            try
            {
                string message = _serialPort.ReadLine();
                Console.WriteLine(message);
            }
            catch (TimeoutException)
            {
                Console.WriteLine("Timeout Exception caught");
            }
        }
    }
}